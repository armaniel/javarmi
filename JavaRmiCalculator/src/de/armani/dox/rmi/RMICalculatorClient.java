package de.armani.dox.rmi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMICalculatorClient {
	private static String COMMAND_ADD = "Add";
	private static String COMMAND_SQR = "Sqr";
	
	public static void main(String[] args) throws NotBoundException, IOException {
		Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
		
		ICalculator calculator = (ICalculator) registry.lookup(RMICalculatorServer.BIND_NAME);
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String combination = null;
		int result;
		while(true) {
			combination =reader.readLine();
			if(combination.equalsIgnoreCase("Over")) {
				break;
			}
			String[] substring = combination.split(",");
			if(substring[0].equalsIgnoreCase(COMMAND_ADD)) {
				result=calculator.add(Integer.parseInt(substring[1]),Integer.parseInt(substring[2]));
				System.out.println("Result of the Operation "+ COMMAND_ADD +" :" + result);
				
			}
			if(substring[0].equalsIgnoreCase(COMMAND_SQR)) {
				result=calculator.sqr(Integer.parseInt(substring[1]));
				System.out.println("Result of the Operation "+ COMMAND_SQR +" :" + result);
				
			}
			
		}
		
	}
	
	public void newFunctionality() {
		/*TO Do*/
	}
}
