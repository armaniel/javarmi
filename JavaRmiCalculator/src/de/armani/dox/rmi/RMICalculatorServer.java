package de.armani.dox.rmi;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMICalculatorServer implements ICalculator {
	public static String BIND_NAME = "CalculatorServer";

	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		
		Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		System.out.println("Registry started on port number: "+ Registry.REGISTRY_PORT);
		
		RMICalculatorServer server = new RMICalculatorServer();
		ICalculator calculator = (ICalculator) UnicastRemoteObject.exportObject(server,0); //export the Object as Distributable
		registry.bind(BIND_NAME,calculator);
		System.out.println("Calculator Server is ready as: "+ BIND_NAME);
	}
	
	@Override
	public int add(int a, int b) throws RemoteException {
		System.out.println("Add Operation...");
		return a+b;
	}

	@Override
	public int sqr(int a) throws RemoteException {
		System.out.println("Sqr Operation...");
		return a*a;
	}
	
	public void newFunctionality() {
		/*TO Do*/
	}

}
