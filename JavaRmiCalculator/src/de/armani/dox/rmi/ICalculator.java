package de.armani.dox.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalculator extends Remote{
	int add(int a,int b) throws RemoteException;
	int sqr(int a) throws RemoteException;
}
